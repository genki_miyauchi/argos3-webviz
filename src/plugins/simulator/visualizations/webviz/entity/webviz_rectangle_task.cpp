/**
 * @file <argos3/plugins/simulator/visualizations/webviz/entity/webviz_rectangle_task.cpp>
 *
 * @author Genki Miyauchi - <g.miyauchi@sheffield.ac.uk>
 *
 */

#include <argos3/plugins/simulator/entities/rectangle_task_entity.h>
#include <argos3/plugins/simulator/entities/led_equipped_entity.h>
#include <argos3/plugins/simulator/visualizations/webviz/webviz.h>

#include <iomanip>
#include <nlohmann/json.hpp>

namespace argos {
  namespace Webviz {

    /****************************************/
    /****************************************/

    class CWebvizOperationGenerateRectangleTaskJSON
        : public CWebvizOperationGenerateJSON {
     public:
      /* cppcheck-suppress unusedFunction */
      nlohmann::json ApplyTo(CWebviz& c_webviz, CRectangleTaskEntity& c_entity) {
        nlohmann::json cJson;

        cJson["type"] = c_entity.GetTypeDescription();
        cJson["id"] = c_entity.GetId();

        /* Get Scale of the box */
        // const argos::CVector2& cScale = c_entity.;
        cJson["scale"]["x"] = c_entity.GetWidthX();
        cJson["scale"]["y"] = c_entity.GetWidthY();
        cJson["scale"]["z"] = c_entity.GetHeight();

        /* Get the position of the box */
        const argos::CVector2& cPosition =
          c_entity.GetPosition();

        /* Add it to json as => position:{x, y, z} */
        cJson["position"]["x"] = cPosition.GetX();
        cJson["position"]["y"] = cPosition.GetY();
        cJson["position"]["z"] = 0;

        /* Get the orientation of the box */
        const argos::CQuaternion& cOrientation = CQuaternion();
          // c_entity.GetEmbodiedEntity().GetOriginAnchor().Orientation;

        cJson["orientation"]["x"] = cOrientation.GetX();
        cJson["orientation"]["y"] = cOrientation.GetY();
        cJson["orientation"]["z"] = cOrientation.GetZ();
        cJson["orientation"]["w"] = cOrientation.GetW();

        /* Get task related information */
        cJson["task"]["demand"] = c_entity.GetDemand();
        cJson["task"]["init_demand"] = c_entity.GetInitDemand();
        cJson["task"]["min_robot"] = c_entity.GetMinRobotNum();
        cJson["task"]["max_robot"] = c_entity.GetMaxRobotNum();

        return cJson;
      }
    };

    REGISTER_WEBVIZ_ENTITY_OPERATION(
      CWebvizOperationGenerateJSON,
      CWebvizOperationGenerateRectangleTaskJSON,
      CRectangleTaskEntity);

  }  // namespace Webviz
}  // namespace argos